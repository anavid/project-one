import {Directive, ElementRef, HostBinding, HostListener, Input, Renderer2} from '@angular/core';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {
  @HostBinding('class.open') opened = false;

  @HostListener('click') toggleOpenClass() {
    this.opened = !this.opened;
    // this.opened ?
    //   this.renderer.addClass(this.elementRef.nativeElement, 'open') :
    //   this.renderer.removeClass(this.elementRef.nativeElement, 'open');
        //   -> hostbinding se brine za open klasu na temelju mijenjanja opened varijable
  }
}
