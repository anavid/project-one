import {Action} from '@ngrx/store';

export const LOGIN_START = '[AUTH] LOGIN START';
export const AUTHENTICATION_FAIL = '[AUTH] LOGIN FAIL';
export const AUTHENTICATION_SUCCESS = '[AUTH] LOGIN';
export const SIGNUP_START = '[AUTH] SIGNUP_START';
export const LOGOUT = '[AUTH] LOGOUT';
export const CLEAR_ERROR = '[AUTH] CLEAR ERROR';
export const AUTO_LOGIN = '[AUTH] AUTO LOGIN';

export class Login implements Action {
  readonly type = AUTHENTICATION_SUCCESS;

  constructor(public payload: {
    userId: string,
    email: string,
    token: string,
    expirationDate: Date,
    redirect: boolean
  }) {}
}

export class Logout implements Action {
  readonly type = LOGOUT;
}

export class LoginStart implements Action {
  readonly type = LOGIN_START;

  constructor(public payload: {email: string, password: string}) {}
}

export class LoginFail implements Action {
  readonly type = AUTHENTICATION_FAIL;

  constructor(public payload: string) {}
}

export class SignUpStart implements Action {
  readonly type = SIGNUP_START;

  constructor(public payload: {email: string, password: string}) {}
}

export class ClearError implements Action {
  readonly type = CLEAR_ERROR;
}

export class AutoLogin implements Action {
  readonly type = AUTO_LOGIN;
}

export type AuthActions = LoginStart
                        | Login
                        | LoginFail
                        | Logout
                        | SignUpStart
                        | ClearError
                        | AutoLogin;
