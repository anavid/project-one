import {Actions, Effect, ofType} from '@ngrx/effects';
import {Injectable} from '@angular/core';
import * as RecipesActions from '../store/recipe.actions';
import {map, switchMap, tap} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {Recipe} from '../../models/recipe.model';

@Injectable()
export class RecipeEffects {

  constructor(private actions$: Actions,
              private http: HttpClient) {}

  @Effect()
  fetchRecipes = this.actions$.pipe(
    ofType(RecipesActions.FETCH_RECIPES),
    switchMap(() => {
      return this.http.get<Recipe[]>(
        'https://recipebook-cf539.firebaseio.com/recipes.json',
      ).pipe(
        map(recipes => {
          return recipes.map(recipe => {
            return {
              ...recipe,
              ingredients: recipe.ingredients ? recipe.ingredients : []
            };
          });
        }),
        map(recipes => {
          return new RecipesActions.SetRecipes(recipes);
        })
      );
    })
  );

  @Effect()
  storeRecipes = this.actions$.pipe(
    ofType(RecipesActions.STORE_RECIPES),
    switchMap((recipes: RecipesActions.StoreRecipes) => {
      return this.http.put(
        'https://recipebook-cf539.firebaseio.com/recipes.json', recipes.payload
      ).pipe(
        map(() => {
          return { type: 'EMPTY ACTION' };
        })
      );
    })
  );
}
