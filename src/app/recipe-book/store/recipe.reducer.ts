import {Recipe} from '../../models/recipe.model';
import * as RecipeActions from './recipe.actions';

export interface State {
  recipes: Recipe[];
}

const initialState: State = {
  recipes: []
};

export function recipeReducer(state = initialState,
                              action: RecipeActions.RecipeActions) {
  switch (action.type) {
    case RecipeActions.SET_RECIPES:
      return {
        ...state,
        recipes: [ ...action.payload ]
      };
    case RecipeActions.ADD_RECIPE:
      return {
        ...state,
        recipes: [ ...state.recipes, action.payload ]
      };
    case RecipeActions.UPDATE_RECIPE: {
      let recipes = [...state.recipes];
      recipes[action.payload.index] = action.payload.recipe;
      return {
        ...state,
        recipes: recipes
      };
    }
    case RecipeActions.DELETE_RECIPE: {
      let recipes = [...state.recipes];
      recipes.splice(action.payload, 1);
      return {
        ...state,
        recipes: recipes
      };
    }
    default:
      return state;
  }
}
