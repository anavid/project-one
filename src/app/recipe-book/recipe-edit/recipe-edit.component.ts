import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {Recipe} from '../../models/recipe.model';
import * as fromApp from '../../store/app.reducer';
import * as RecipesActions from '../store/recipe.actions';
import {Store} from '@ngrx/store';
import {map, take} from 'rxjs/operators';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {
  id: number;
  editMode = false;
  editRecipeForm: FormGroup;
  recipe: Recipe = new Recipe('', '', '', []);

  constructor(private route: ActivatedRoute,
              private router: Router,
              private store: Store<fromApp.AppState>) { }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params.id;
        this.editMode = params.id != null;
        this.initForm();
      });
  }

  private initForm() {
    let ingredientArray = new FormArray([]);
    if (this.editMode) {
      this.getRecipe();
      if (this.recipe.ingredients) {
        for (let ingredient of this.recipe.ingredients) {
          ingredientArray.push(
            new FormGroup({
              name: new FormControl(ingredient.name, Validators.required),
              amount: new FormControl(ingredient.amount, [Validators.required,
                    Validators.pattern('^[1-9]+[0-9]*$')])
            })
          );
        }
      }
    }
    this.editRecipeForm = new FormGroup({
      name: new FormControl(this.recipe.name, Validators.required),
      imagePath: new FormControl(this.recipe.imagePath, Validators.required),
      description: new FormControl(this.recipe.description, Validators.required),
      ingredients: ingredientArray
    });
  }

  onAddIngredient() {
    (this.editRecipeForm.get('ingredients') as FormArray).push(
      new FormGroup({
        name: new FormControl(null, Validators.required),
        amount: new FormControl(null, [Validators.required,
          Validators.pattern(/^[1-9]+[0-9]*$/)])
      })
    );
  }

  private getRecipe() {
    this.store.select('recipes').pipe(
      take(1),
      map(recipesData => recipesData.recipes[this.id - 1])
    ).subscribe(recipe => this.recipe = recipe);
  }

  onSubmit() {
    if (this.editMode) {
      this.store.dispatch(new RecipesActions.UpdateRecipe({recipe: this.editRecipeForm.value, index: this.id - 1}));
      this.router.navigate(['/recipes', this.id]);
    } else {
      this.store.dispatch(new RecipesActions.AddRecipe(this.editRecipeForm.value));
      this.store.select('recipes').pipe(
        take(1),
        map(recipesData => {
          this.router.navigate(['/recipes', recipesData.recipes.length]);
        })
      );
    }
  }

  get ingredientControls() {
    return (this.editRecipeForm.get('ingredients') as FormArray).controls;
  }

  cancelEditing() {
    this.router.navigate(['../']);
  }

  onDeleteIngredient(index: number) {
    (this.editRecipeForm.get('ingredients') as FormArray).removeAt(index);
  }
}
