import {Component, OnInit} from '@angular/core';
import {Recipe} from '../../models/recipe.model';
import {Ingredient} from '../../models/ingredient.model';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import * as ShoppingListActions from '../../shopping-list/store/shopping-list.actions';
import * as RecipesActions from '../store/recipe.actions';
import * as fromApp from './../../store/app.reducer';
import {map, switchMap, take} from 'rxjs/operators';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {
  recipeData: Recipe;
  recipeId: number;
  openDropdown = false;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private store: Store<fromApp.AppState>) { }

  ngOnInit() {
    this.route.params
      .pipe(
        map( params => +params.id ),
        switchMap(id => {
          this.recipeId = --id;
          return this.store.select('recipes')
            .pipe(
              map(recipeData => recipeData.recipes),
              map(recipes => {
                return recipes[this.recipeId];
              })
            );
      })).subscribe(
            recipe => {
              this.recipeData = recipe;
            }
          );
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.store.dispatch(new ShoppingListActions.AddIngredients(ingredients));
  }

  deleteRecipe(recipeId: number) {
    this.store.dispatch(new RecipesActions.DeleteRecipe(recipeId - 1));
    this.router.navigate(['/recipes']);
  }
}
