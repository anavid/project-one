import {Ingredient} from '../../models/ingredient.model';
import * as ShoppingListActions from './shopping-list.actions';

export interface State {
  ingredients: Ingredient[];
  editedIngredientIndex: number;
}

const initialState: State = {
  ingredients: [
    new Ingredient('IngredientOne', 10),
    new Ingredient('IngredientTwo', 5),
  ],
  editedIngredientIndex: -1
};

export function shoppingListReducer(state: State = initialState,
                                    action: ShoppingListActions.ShoppingListActions) {
  switch (action.type) {
    case ShoppingListActions.ADD_INGREDIENT:
      return {
        ...state,
        ingredients: [...state.ingredients, action.payload]
      };
    case ShoppingListActions.ADD_INGREDIENTS:
      return {
        ...state,
        ingredients: [...state.ingredients, ...action.payload]
      };
    case ShoppingListActions.UPDATE_INGREDIENT: {
      let ingredients = [ ...state.ingredients ];
      ingredients[state.editedIngredientIndex] = action.payload;
      return {
        ...state,
        ingredients: ingredients,
        editedIngredientIndex: -1,
      };
    }
    case ShoppingListActions.DELETE_INGREDIENT: {
      let ingredients = state.ingredients.filter( (ing, i) => {
        return i !== state.editedIngredientIndex;
      });
      return {
        ...state,
        ingredients: ingredients,
        editedIngredientIndex: -1
      };
    }
    case ShoppingListActions.START_EDIT: {
      return {
        ...state,
        editedIngredientIndex: action.payload.index,
      };
    }
    case ShoppingListActions.STOP_EDIT:
      return {
       ...state,
       editedIngredientIndex: -1
      };
    default:
      return state;
  }
}

